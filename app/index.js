'use strict'

let glob = require('globby')

class Application {
  bootstrap (system) {

    return glob([`${__dirname}/**/*/index.js`, `!${__dirname}/**/core/*`]).then((apiVersions) => {
      return Promise.each(apiVersions, (version) => {
        let ApiVersion = require(version)
        let apiVersion = new ApiVersion()

        return apiVersion.bootstrap(system.httpServer)
      })
    })
  }
}

module.exports = Application
