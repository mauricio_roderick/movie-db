'use strict'

const API_VERSION = '1.0.0'

let _ = require('lodash')
let path = require('path')
let glob = require('globby')
let expressValidator = require('express-validator')
let versionCore = require('./core')

class ResourceContainer {
  bootstrap (httpServer) {

    httpServer.use(expressValidator(versionCore.libraries.validator))
    
    return this.bootstrapModels()
      .then(() => this.bootstrapRoutes(httpServer))
  }

  bootstrapModels () {
    return glob([`${__dirname}/**/model.js`]).then((models) => {
      return Promise.map(models, (model) => {
        return require(model)(versionCore)
      })
    })
  }

  bootstrapRoutes (httpServer) {
    return glob([`${__dirname}/**/route.js`]).then((routeFiles) => {
      return Promise.each(routeFiles, (resource) => require(resource)(httpServer, versionCore))
    })
  }
}

module.exports = ResourceContainer
