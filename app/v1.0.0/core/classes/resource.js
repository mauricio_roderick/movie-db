'use strict'

let _ = require('lodash')
let errs = require('restify-errors')

class Resource {
  constructor (Model, options = {}) {
    this.Model = Model
    this.options = Object.assign({
      resourceLabel: '',
      excludedProperties: [],
      excludedDetailLinks: [],
      skip: 0,
      page: 0,
      limit: 25,
      sort: '',
      select: '',
      useLean: true,
      enableDeepPopulate: false
    }, options)
  }

  aggregate (options = {}) {
    let {agrPipeline, page, sort, limit} = options
    let {page: thisPage, limit: thisLimit, sort: thisSort} = this.options

    page = _.isFinite(parseInt(page)) ? parseInt(page) || thisPage : thisPage
    sort = options.sort || thisSort
    limit = _.isFinite(parseInt(limit)) ? parseInt(limit) || thisLimit : thisLimit
    let skip = (limit * page)
    let countPipe = {
      $group: {
        _id: null,
        count: {
          $sum: 1
        }
      }
    }

    return new Promise((resolve, reject) => {
      if (_.isEmpty(agrPipeline)) {
        agrPipeline = [{
          $match: {}
        }]
      }

      if (!Array.isArray(agrPipeline)) {
        agrPipeline = [agrPipeline]
      }

      let query = this.Model.aggregate(agrPipeline)
      let countQuery = agrPipeline.concat([countPipe])
      countQuery = this.Model.aggregate(countQuery)

      if (!_.isEmpty(sort)) {
        query.sort(sort)
      }

      if (!_.isNil(skip) && _.isNumber(skip)) {
        query.skip(skip)
      } else {
        query.skip(0)
      }

      if (!_.isNil(limit) && _.isNumber(limit)) {
        query.limit((limit > 100) ? 100 : limit)
      }

      countQuery.then((countResult) => {
        let count = (!_.isEmpty(countResult)) ? countResult.pop().count : 0

        return query.then(docs => {
          let populateResult = Promise.resolve(docs)

          if (!_.isEmpty(options.expand)) {
            if (this.options.enableDeepPopulate === true) {
              populateResult = this.Model.deepPopulate(docs, options.expand)
            } else {
              populateResult = this.Model.populate(docs, options.expand)
            }
          }

          return populateResult
        })
        .then(docs => {
          if (_.isFunction(options.projection)) {
            return Promise.map(docs, doc => {
              return options.projection(doc)
            }).then(projectedDocs => {
              resolve({
                totalRecords: count,
                totalPages: (Math.ceil(count / limit) - 1 < 0) ? 0 : Math.ceil(count / limit) - 1,
                currentPage: page,
                docsPerPage: limit,
                data: projectedDocs
              })
            })
          } else {
            resolve({
              totalRecords: count,
              totalPages: (Math.ceil(count / limit) - 1 < 0) ? 0 : Math.ceil(count / limit) - 1,
              currentPage: page,
              docsPerPage: limit,
              data: docs
            })
          }
        }).catch(reject)
      }).catch(reject)
    })
  }

  search (options = {}) {
    let page = _.isFinite(parseInt(options.page)) ? parseInt(options.page) || this.options.page : this.options.page
    let sort = options.sort || this.options.sort
    let limit = _.isFinite(parseInt(options.limit)) ? parseInt(options.limit) || this.options.limit : this.options.limit
    let select = options.select || this.options.select
    let skip = (limit * page)

    return new Promise((resolve, reject) => {
      let query = this.Model.find({})
      let countQuery = this.Model.find({})

      if (!_.isEmpty(options.filter) && _.isPlainObject(options.filter)) {
        query.where(options.filter)
        countQuery.where(options.filter)
      }

      if (!_.isEmpty(select) && _.isString(select)) {
        query.select(select)
      }

      if (!_.isEmpty(sort)) {
        query.sort(sort)
      }

      if (!_.isNil(limit) && _.isNumber(limit)) {
        query.limit((limit > 100) ? 100 : limit)
      }

      if (!_.isNil(skip) && _.isNumber(skip)) {
        query.skip(skip)
      } else {
        query.skip(0)
      }

      if (!_.isEmpty(options.expand)) {
        if (this.options.enableDeepPopulate === true) {
          query = query.deepPopulate(options.expand)
        } else {
          query.populate(options.expand)
        }
      }

      if (options.useLean === true) {
        query.lean()
      }

      countQuery.count().then((count) => {
        return query.exec().then(docs => {
          if (_.isFunction(options.projection)) {
            return Promise.map(docs, doc => {
              return options.projection(doc)
            }).then(projectedDocs => {
              resolve({
                totalRecords: count,
                totalPages: (Math.ceil(count / limit) - 1 < 0) ? 0 : Math.ceil(count / limit) - 1,
                currentPage: page,
                docsPerPage: limit,
                data: projectedDocs
              })
            })
          } else {
            resolve({
              totalRecords: count,
              totalPages: (Math.ceil(count / limit) - 1 < 0) ? 0 : Math.ceil(count / limit) - 1,
              currentPage: page,
              docsPerPage: limit,
              data: docs
            })
          }
        }).catch(reject)
      }).catch(reject)
    })
  }

  read (options = {}) {
    let select = options.select || this.options.select

    return new Promise((resolve, reject) => {
      let query = this.Model.findOne(options.filter)

      if (!_.isEmpty(select) && _.isString(select)) {
        query.select(select)
      }

      if (!_.isEmpty(options.expand) && _.isString(options.expand)) {
        if (this.options.enableDeepPopulate === true) {
          query.deepPopulate(options.expand)
        } else {
          query.populate(options.expand)
        }
      }

      query.exec().then((doc) => {
        if (_.isEmpty(doc)) {
          return reject(new errs.ResourceNotFoundError({
            code: 'ResourceNotFoundError',
            message: 'The requested resource doesn\'t exist.'
          }))
        }

        if (_.isFunction(options.projection)) {
          options.projection(doc).then(projectedDoc => {
            resolve(projectedDoc)
          })
        } else {
          resolve(doc)
        }
      }).catch(reject)
    })
  }

  create (options = {}) {
    return new Promise((resolve, reject) => {
      let model = new this.Model(options.data)

      model.set(options.data)

      let fields = model.modifiedPaths()

      model.save().then((doc) => {
        if (!_.isEmpty(options.expand) && this.options.enableDeepPopulate === true) {
          this.Model.deepPopulate(doc, options.expand).then(populatedDoc => {
            if (_.isFunction(options.projection)) {
              resolve(options.projection(populatedDoc))
            } else {
              resolve(populatedDoc)
            }
          })
        } else if (!_.isEmpty(options.expand)) {
          this.Model.populate(doc, options.expand).then(populatedDoc => {
            if (_.isFunction(options.projection)) {
              resolve(options.projection(populatedDoc))
            } else {
              resolve(populatedDoc)
            }
          })
        } else {
          if (_.isFunction(options.projection)) {
            resolve(options.projection(doc))
          } else {
            resolve(doc)
          }
        }
      }).catch(reject)
    })
  }

  update (options = {}) {
    return new Promise((resolve, reject) => {
      let query = this.Model.findOne(options.filter)

      query.exec().then((doc) => {
        if (_.isEmpty(doc)) {
          return reject(new errs.ResourceNotFoundError({
            code: 'ResourceNotFoundError',
            message: 'The requested resource doesn\'t exist.'
          }))
        }

        let oldDoc = (!_.isPlainObject(doc)) ? doc.toObject() : doc

        doc.set(options.data)

        if (!_.isEmpty(options.modifiedPaths)) {
          options.modifiedPaths.forEach((path) => {
            if (!_.isNil(_.get(options, `data.${path}`))) {
              doc.set(path, undefined)
            }

            doc.markModified(path)
          })
        }

        let fields = doc.modifiedPaths()

        doc.save().then((updatedDoc) => {
          if (!_.isEmpty(options.expand) && this.options.enableDeepPopulate === true) {
            this.Model.deepPopulate(updatedDoc, options.expand).then(populatedDoc => {
              if (_.isFunction(options.projection)) {
                resolve(options.projection(populatedDoc))
              } else {
                resolve(populatedDoc)
              }
            })
          } else if (!_.isEmpty(options.expand)) {
            this.Model.populate(updatedDoc, options.expand).then(populatedDoc => {
              if (_.isFunction(options.projection)) {
                resolve(options.projection(populatedDoc))
              } else {
                resolve(populatedDoc)
              }
            })
          } else {
            if (_.isFunction(options.projection)) {
              resolve(options.projection(updatedDoc))
            } else {
              resolve(updatedDoc)
            }
          }
        }).catch(reject)
      }).catch(reject)
    })
  }

  remove (options = {}) {
    return new Promise((resolve, reject) => {
      let query = this.Model.findOne(options.filter)

      query.exec().then((doc) => {
        if (_.isEmpty(doc)) {
          return reject(new errs.ResourceNotFoundError({
            code: 'ResourceNotFoundError',
            message: 'The requested resource doesn\'t exist.'
          }))
        }

        doc.remove().then(() => {
          resolve()
        }).catch(reject)
      }).catch(reject)
    })
  }

  getListProjection (query, data) {
    let {resourceLabel, router, resource} = this.options

    let links = [{
      href: router.render(`${resource}.create`),
      rel: `${resource}.create`,
      method: 'POST',
      description: `Create a new ${resourceLabel}.`
    }]

    Object.assign(query, {
      page: data.currentPage || 0
    })

    let nextQueryData = _.clone(query)
    nextQueryData.page += 1
    let nextPage = {
      href: router.render(`${resource}.search`, {}, nextQueryData),
      rel: 'next',
      method: 'GET',
      description: 'Continue to the next page.'
    }

    let previousQueryData = _.clone(query)
    previousQueryData.page -= 1
    let prevPage = {
      href: router.render(`${resource}.search`, {}, previousQueryData),
      rel: 'previous',
      method: 'GET',
      description: 'Go back to the previous page.'
    }

    if (data.currentPage === 0 && data.totalPages > 1) {
      links.push(nextPage)
    } else if (data.currentPage > 0 && data.currentPage === data.totalPages) {
      links.push(prevPage)
    } else if (!(data.totalRecords === 0 || data.totalPages === 0)) {
      links.push(nextPage)
      links.push(prevPage)
    }

    _.set(data, '_links', links)

    return Promise.resolve(data)
  }

  getDetailLinks (linkOptions = {}) {
    let {id, query} = linkOptions
    let {resourceLabel, resource, router} = this.options

    return Promise.resolve([{
      href: router.render(`${resource}.read`, {id}, query),
      rel: 'self',
      method: 'GET',
      description: `Retrieve ${resourceLabel} information.`
    }, {
      href: router.render(`${resource}.update`, {id}),
      rel: 'self.update',
      method: 'PATCH',
      description: `Update ${resourceLabel} information.`
    }, {
      href: router.render(`${resource}.delete`, {id}),
      rel: 'self.delete',
      method: 'DELETE',
      description: `Delete ${resourceLabel}.`
    }])
  }

  getDetailProjection (query) {
    let {excludedDetailLinks, excludedProperties} = this.options
    let self = this

    return function (doc) {
      if (!_.isPlainObject(doc)) doc = doc.toObject()

      return self.getDetailLinks({
        id: doc._id,
        query: _.pick(query, ['select', 'expand'])
      }).then(links => {
        return Promise.resolve(links.filter((link) => {
          return (!(_.includes(excludedDetailLinks, link.method) || _.isNull(link.href)))
        }))
      }).then(links => {
        if (!_.isEmpty(excludedProperties)) {
          _.unset(doc, excludedProperties)
        }

        _.set(doc, '_links', links)

        return Promise.resolve(doc)
      })
    }
  }
}

module.exports = Resource
