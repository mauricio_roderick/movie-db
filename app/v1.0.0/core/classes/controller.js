'use strict'

let _ = require('lodash')
let raven = require('raven')
let errs = require('restify-errors')

class Controller {
  constructor (service) {
    this.service = service
  }

  search (options = {}) {
    return (req, res, next) => {
      let opt = _.cloneDeep(options)

      if (_.isEmpty(opt.projection)) {
        Object.assign(opt, { 
          projection: this.service.getDetailProjection(req.query)
        })
      }

      return this.service.search(Object.assign(_.clone(req.query), opt)).then((data) => {
        return this.service.getListProjection(req.query, data)
      }).then((data) => {
        res.json(200, data)
        next(false)

        return Promise.resolve(data)
      }).catch((err) => {
        return this.handleRouteError(req, res, next, err)
      })
    }
  }

  read (options = {}) {
    return (req, res, next) => {
      let opt = _.cloneDeep(options)

      if (_.isEmpty(opt.projection)) {
        Object.assign(opt, {
          projection: this.service.getDetailProjection(req.query)
        })
      }

      if (_.isEmpty(opt.filter)) {
        Object.assign(opt, {
          filter: {
            _id: req.params.id
          }
        })
      }

      return this.service.read(Object.assign(_.clone(req.query), opt)).then((data) => {
        res.json(200, data)
        next(false)

        return Promise.resolve(data)
      }).catch((err) => {
        return this.handleRouteError(req, res, next, err)
      })
    }
  }

  create (options = {}) {
    return (req, res, next) => {
      let opt = _.cloneDeep(options)

      if (_.isEmpty(opt.projection)) {
        Object.assign(opt, {
          projection: this.service.getDetailProjection(req.query)
        })
      }

      if (_.isEmpty(opt.user) && !_.isEmpty(req.user)) {
        Object.assign(opt, {
          user: {
            _id: req.user._id,
            account: req.user.account
          }
        })
      }

      if (_.isEmpty(opt.data)) {
        Object.assign(opt, {
          data: req.body
        })
      }

      if (!_.isEmpty(req.query)) {
        Object.assign(opt, req.query)
      }

      return this.service.create(opt).then((doc) => {
        res.json(201, doc)
        next(false)

        return Promise.resolve(doc)
      }).catch((err) => {
        return this.handleRouteError(req, res, next, err)
      })
    }
  }

  update (options = {}) {
    return (req, res, next) => {
      let opt = _.cloneDeep(options)

      if (_.isEmpty(opt.projection)) {
        Object.assign(opt, {
          projection: this.service.getDetailProjection(req.query)
        })
      }

      if (_.isEmpty(opt.user) && !_.isEmpty(req.user)) {
        Object.assign(opt, {
          user: {
            _id: req.user._id,
            account: req.user.account
          }
        })
      }

      if (_.isEmpty(opt.data)) {
        Object.assign(opt, {
          data: req.body
        })
      }

      if (_.isEmpty(opt.filter)) {
        Object.assign(opt, {
          filter: {
            _id: req.params.id
          }
        })
      }

      if (!_.isEmpty(req.query)) {
        Object.assign(opt, req.query)
      }

      return this.service.update(opt).then((doc) => {
        res.json(200, doc)
        next(false)

        return Promise.resolve(doc)
      }).catch((err) => {
        return this.handleRouteError(req, res, next, err)
      })
    }
  }

  remove (options = {}) {
    return (req, res, next) => {
      let opt = _.cloneDeep(options)

      if (_.isEmpty(opt.filter)) {
        Object.assign(opt, {
          filter: {
            _id: req.params.id
          }
        })
      }

      return this.service.remove(opt).then(() => {
        res.json(204)
        next(false)

        return Promise.resolve()
      }).catch((err) => {
        return this.handleRouteError(req, res, next, err)
      })
    }
  }

  handleRouteError (req, res, next, err) {
    req.log.warn(err)

    if (_.get(err, 'name') === 'MongoError' && _.get(err, 'code') === 11000) {
      next(new errs.ConflictError({
        code: 'ConflictError',
        message: 'A duplicate record already exists.'
      }))
    } else if (Array.isArray(err)) {
      next(new errs.BadRequestError({
        code: 'BadRequestError',
        message: 'Request validation has failed.',
        context: {
          details: err
        }
      }))
    } else if (err instanceof errs.HttpError || err instanceof errs.RestError) {
      next(err)
    } else {
      raven.captureException(err)

      next(new errs.InternalServerError({
        code: 'InternalServerError',
        message: 'An unexpected error has occurred. Kindly contact support for more information.'
      }))
    }
  }

  validate (schema = {}) {
    return (req, res, next) => {
      let validationSchema = _.isFunction(schema) ? schema(req) : schema

      if (_.isEmpty(validationSchema)) return next()

      let fieldKeys = Object.keys(validationSchema)

      return Promise.map(fieldKeys, fieldKey => {
        req.sanitize(fieldKey).customTrim()
        req.check(validationSchema)

        return Promise.resolve()
      }).then(() => req.getValidationResult())
      .then((result) => {
        let validationErros

        if (!result.isEmpty()) {
          validationErros = new errs.BadRequestError({
            code: 'BadRequestError',
            message: 'Request validation has failed.',
            context: {
              details: result.array({
                onlyFirstError: true
              })
            }
          })
        }

        next(validationErros)

        return Promise.resolve()
      }).catch((err) => {
        this.handleRouteError(req, res, next, err)
        return Promise.resolve()
      })
    }
  }
}

module.exports = Controller
