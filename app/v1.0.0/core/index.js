'use strict'

let fs = require('fs')
let path = require('path')
let Chance = require('chance')
let config = require('./config.json')

module.exports = {
  config: require('./config.json'),
  utils: {
  	chance: new Chance()
  },
  libraries: {
  	validator: require('./libraries/validator')
  },
  classes: {
    Resource: require('./classes/resource'),
    Controller: require('./classes/controller')
  }
}
