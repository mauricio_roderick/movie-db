'use strict'

let _ = require('lodash')
let mongoose = require('mongoose')

module.exports = {
  customSanitizers: {
    customTrim: (fieldValue) => {
      if (_.isString(fieldValue)) {
        return fieldValue.trim()
      } else if (Array.isArray(fieldValue)) {
        fieldValue = fieldValue.map((item) => {
          return _.isString(item) ? item.trim() : item
        })
      }

      return fieldValue
    }
  },
  customValidators: {
    isArray: (value) => {
      return Array.isArray(value)
    },
    isJsonObject: (value) => {
      return _.isPlainObject(value)
    },
    isUniqueRecord: (value, options = {}) => {
      return new Promise((resolve, reject) => {
        let model = mongoose.model(options.model)
        let pluginFiler = {}

        pluginFiler[options.field] = value

        if (options.excludedRecordId) {
          let id = Array.isArray(options.excludedRecordId) ? options.excludedRecordId : [options.excludedRecordId]
          pluginFiler._id = {
            $nin: id
          }
        }

        model.find(pluginFiler, (error, docs) => {
          if (error) {
            reject(error)
          } else if (docs.length > 0) {
            reject(new Error('Please specify a unique record.'))
          } else {
            resolve()
          }
        })
      })
    },
    groupRecordExist: (recordId, modelName, expectedNumOfResults = 1, filter = {}) => {
      return new Promise((resolve, reject) => {
        let model = mongoose.model(modelName)

        if (_.isEmpty(filter)) {
          let recordIds = (!Array.isArray(recordId)) ? [recordId] : recordId
          filter = {
            _id: {
              $in: recordIds
            }
          }

          expectedNumOfResults = recordIds.length
        }

        model.count(filter, (error, recordCount) => {
          if (error) {
            reject(error)
          } else if (recordCount !== expectedNumOfResults) {
            reject(new Error('Invalid value was fround from the list.'))
          }

          resolve()
        })
      })
    },
    recordDoesntExist: (value, options) => {
      return new Promise((resolve, reject) => {
        let {model, filter} = options
        model = mongoose.model(model)

        model.findOne(filter, (error, doc) => {
          if (error) {
            reject(error)
          } else if (doc) {
            reject(new Error('Record already exist.'))
          } else {
            resolve()
          }
        })
      })
    },
    recordExist: (value, options) => {
      return new Promise((resolve, reject) => {
        let {model, filter} = options

        model = mongoose.model(model)
        if (_.isEmpty(filter)) {
          filter = {
            _id: value
          }
        }

        model.findOne(filter, (error, doc) => {
          if (error) {
            reject(error)
          } else if (!doc) {
            reject(new Error(`Record doesn't exist.`))
          } else {
            resolve()
          }
        })
      })
    },
    groupIsIn: (group, allowedValues) => {
      if (!Array.isArray(group) || !Array.isArray(allowedValues)) return false

      return group.every((item) => {
        return allowedValues.includes(item)
      })
    }
  }
}
