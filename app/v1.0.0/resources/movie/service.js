'use strict'

const resourceConfig  = require('./config')

let _ = require('lodash')
let mongoose = require('mongoose')
let errs = require('restify-errors')

module.exports = (router, core) => {
  let Account = mongoose.model(`${resourceConfig.modelName}-v${core.config.apiVersion}`)
  let service = new core.classes.Resource(Account, {
    router: router,
    resource: resourceConfig.name,
    resourceLabel: 'account',
    sort: 'name'
  })

  return service
}
