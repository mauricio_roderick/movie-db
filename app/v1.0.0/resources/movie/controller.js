'use strict'

let _ = require('lodash')
let service = require('./service')

module.exports = ({router}, core) => {
  let movieService = service(router, core)
  let baseController = new core.classes.Controller(movieService)

  class Controller {
    constructor() {
      Object.assign(this, _.pick(baseController, ['validate', 'handleRouteError']))
    }

    search() {
      return (req, res, next) => {
        let options = {}

        baseController.search(options)(req, res, next)
      }
    }

    create() {
      return (req, res, next) => {
        let {title, director, productionCompany} = req.body
        let options = {
          data: {
            title,
            director,
            productionCompany
          }
        }

        baseController.create(options)(req, res, next)
      }
    }

    update() {
      return (req, res, next) => {
        let options = {
          data: _.omit(req.body, ['_id']),
          filter: {
            _id: req.params.id
          }
        }

        baseController.update(options)(req, res, next)
      } 
    }

    remove () {
      return (req, res, next) => {
        let options = {
          filter: {
            _id: req.params.id
          }
        }

        baseController.remove(options)(req, res, next)
      }
    }
  }

  return new Controller()
}
