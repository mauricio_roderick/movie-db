'use strict'

const resourceConfig  = require('./config')
let mongoose = require('mongoose')

module.exports = ({config, utils}) => {
  return new Promise(resolve => {
    let Schema = mongoose.Schema

    let movieSchema = new Schema({
      _id: {
        type: String,
        label: 'ID',
        required: 'Kindly specify valid movie ID.',
        trim: true,
        default: () => utils.chance.guid()
      },
      title: {
        type: String,
        required: 'Kindly specify movie name.',
        trim: true,
        unique: true
      },
      director: {
        type: String,
        required: 'Kindly specify movie director.',
        trim: true
      },
      productionCompany: {
        type: String,
        required: 'Kindly specify production company.',
        trim: true
      }
    }, {
      emitIndexErrors: true,
      minimize: false,
      retainKeyOrder: true
    })

    mongoose.model(`${resourceConfig.modelName}-v${config.apiVersion}`, movieSchema, resourceConfig.name)

    resolve()
  })
}
