'use strict'

const resourceConfig  = require('./config')
let controller = require('./controller')

module.exports = (server, core) => {
  return new Promise(resolve => {
    let validations = require('./validations')(core)
    let controller = require('./controller')(server, core)

    server.get({
      name: `${resourceConfig.name}.search`,
      path: `/${resourceConfig.name}`,
      version: core.apiVersion
    }, controller.search())

    server.post({
      name: `${resourceConfig.name}.create`,
      path: `/${resourceConfig.name}`,
      version: core.apiVersion
    }, controller.validate(validations.create),
    controller.create())

    server.patch({
      name: `${resourceConfig.name}.update`,
      path: `/${resourceConfig.name}/:id`,
      version: core.apiVersion
    }, controller.validate(validations.update), controller.update())

    server.del({
      name: `${resourceConfig.name}.delete`,
      path: `/${resourceConfig.name}/:id`,
      version: core.apiVersion
    }, controller.validate(validations.remove), controller.remove())

    resolve()
  })
}
