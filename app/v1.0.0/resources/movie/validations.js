'use strict'

let _ = require('lodash')
const resourceConfig  = require('./config')

module.exports = ({config}) => {
  let idValidation = {
    in: 'params',
    notEmpty: true,
    isUUID: {
      options: 5,
      errorMessage: 'Kindly specify a valid record id.'
    },
    errorMessage: 'Kindly specify a valid record id.'
  }

  let validationSchema = {
    title: {
      in: 'body',
      notEmpty: true,
      isLength: {
        options: [{max: 50}],
        errorMessage: 'Name must not exceed more than 50 characters.'
      },
      errorMessage: 'Kindly specify a valid movie title.'
    },
    director: {
      in: 'body',
      notEmpty: true,
      isLength: {
        options: [{max: 50}],
        errorMessage: 'Director must not exceed more than 50 characters.'
      },
      errorMessage: 'Kindly specify a movie director.'
    },
    productionCompany: {
      in: 'body',
      notEmpty: true,
      isLength: {
        options: [{max: 50}],
        errorMessage: 'Production company must not exceed more than 50 characters.'
      },
      errorMessage: 'Kindly specify a production company.'
    }
  }

  return {
  	search: {

    },
    create: (req) => {
      let createSchema = _.cloneDeep(validationSchema)

      createSchema.title.recordDoesntExist = {
        errorMessage: 'A movie with the title "%0" already exists.',
        options: {
          model: `${resourceConfig.modelName}-v${config.apiVersion}`,
          filter: {
            title: req.body.title
          }
        }
      }

      return createSchema
    },
    update: (req) => {
      let updateSchema = _.cloneDeep(validationSchema)

      updateSchema.title.recordDoesntExist = {
        errorMessage: 'A movie with the title "%0" already exists.',
        options: {
          model: `${resourceConfig.modelName}-v${config.apiVersion}`,
          filter: {
            _id: {
              $ne: req.params.id
            },
            title: req.body.title
          }
        }
      }

      Object.assign(updateSchema, {
        director: {
          in: 'body',
          notEmpty: true,
          isLength: {
            options: [{max: 50}],
            errorMessage: 'Director must not exceed more than 50 characters.'
          },
          errorMessage: 'Kindly specify a movie director.'
        },
        productionCompany: {
          in: 'body',
          notEmpty: true,
          isLength: {
            options: [{max: 50}],
            errorMessage: 'Production company must not exceed more than 50 characters.'
          },
          errorMessage: 'Kindly specify a production company.'
        }
      })

      Object.keys(updateSchema).forEach((field) => {
        if (_.isEmpty(updateSchema[field].optional)) {
          updateSchema[field].optional = true
        }
      })

      updateSchema.id = idValidation
      return updateSchema
    },
    remove: {
      id: idValidation
    } 
  }
}