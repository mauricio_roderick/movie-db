'use strict'

let Database = require('./database')
let HttpServer = require('./http-server')

class Components {
  constructor (logger) {
    this.database = new Database(logger)
    this.httpServer = new HttpServer()
  }
}

module.exports = Components
