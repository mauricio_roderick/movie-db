'use strict'

let _ = require('lodash')
let mongoose = require('mongoose')

class Database {
  constructor (logger) {
    this.logger = logger
  }

  bootstrap () {
    let options = {
      useMongoClient: true,
      keepAlive: 10000,
      promiseLibrary: Promise
    }
    
    let connection = mongoose.connect(process.env.MONGO_URL, options)

    return connection.then(() => {
      console.log('Connected to system database.')
    })
  }

  close () {
    this.logger.info('Closing Database connection.')

    return new Promise((resolve, reject) => {
      mongoose.disconnect((err) => {
        if (!_.isNil(err)) {
          reject(err)
        } else {
          this.logger.info('Database connection closed.')

          resolve()
        }
      })
    })
  }
}

module.exports = Database
