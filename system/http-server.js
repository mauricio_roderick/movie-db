'use strict'

let _ = require('lodash')
// let os = require('os')
// let cors = require('restify-cors-middleware')
// let raven = require('raven')
// let helmet = require('helmet')
// let Chance = require('chance')
// let chance = new Chance()
let restify = require('restify')
let errs = require('restify-errors')
// let pinoHttp = require('pino-http')
// let passport = require('passport')
// let responseTime = require('response-time')
let genericErrorResponse = new errs.InternalServerError('An unexpected error has occurred. Kindly contact support for more information.')

class HttpServer {
  constructor () {
  }

  bootstrap () {
    return new Promise((resolve) => {
      let server = restify.createServer({
        name: process.env.API_NAME,
        version: process.env.DEFAULT_API_VERSION,
        handleUncaughtExceptions: false, // produces deprecation warning when set to true
        formatters: {
          'application/json': (req, res, body) => {
            if (body instanceof Error) {
              body = {
                code: body.code,
                message: body.message,
                details: _.get(body.context, 'details', [])
              }
            } else if (Buffer.isBuffer(body)) {
              body = body.toString('base64')
            }

            return JSON.stringify(body)
          }
        }
      })

      server.use(restify.plugins.queryParser())
      server.use(restify.plugins.bodyParser({
        maxBodySize: 5000000,
        mapParams: false,
        overrideParams: false,
        keepExtensions: true,
        hash: 'sha1'
      }))
      
      server.get('/', (req, res, next) => {
        res.send('App running!')
        next()
      })

      // let corsHandler = cors({
      //   origins: ['*'],
      //   allowHeaders: ['Host', 'Accept', 'Accept-Version', 'Authorization', 'Content-Type', 'Content-Length', 'Content-MD5', 'Date', 'Api-Version', 'Response-Time', 'X-Forwarded-For', 'X-Forwarded-Proto', 'X-Real-IP', 'responseType'],
      //   exposeHeaders: ['X-RateLimit-Limit', 'X-RateLimit-Remaining', 'X-RateLimit-Reset', 'Retry-After', 'X-Response-Time', 'Api-Version', 'Request-Id', 'Response-Time']
      // })

      // server.pre(restify.pre.userAgentConnection())
      // server.pre(corsHandler.preflight)

      // // Response time tracking
      // server.use(responseTime())

      // server.use(logger)
      // server.use(corsHandler.actual)
      // server.use(restify.plugins.acceptParser(server.acceptable))
      // server.use(restify.plugins.fullResponse())

      // server.use(restify.plugins.queryParser())
      // server.use(restify.plugins.bodyParser({
      //   maxBodySize: 5000000,
      //   mapParams: false,
      //   mapFiles: false,
      //   overrideParams: false,
      //   keepExtensions: true,
      //   // uploadDir: os.tmpdir(),
      //   hash: 'sha1'
      // }))

      // // Helmet Security Middleware
      // if (process.env.NODE_ENV === 'production') {
      //   server.use(helmet.frameguard({action: 'deny'}))
      //   server.use(helmet.hidePoweredBy())
      //   server.use(helmet.ieNoOpen())
      //   server.use(helmet.noSniff())
      //   server.use(helmet.xssFilter())
      // }

      // server.use(passport.initialize())

      // server.on('uncaughtException', (req, res, route, err) => {
      //   req.log.error(err)

      //   if (process.env.NODE_ENV === 'production') {
      //     raven.captureException(err, {
      //       extra: {
      //         user: req.user,
      //         method: req.method,
      //         headers: req.headers,
      //         route: route,
      //         params: req.params,
      //         query: req.query,
      //         body: req.body
      //       }
      //     })
      //   }

      //   return res.send(genericErrorResponse)
      // })

      server.on('after', (req, res, route, err) => {
        if (!_.isNil(err) && err !== false) {
          req.log.error(err)

          if (process.env.NODE_ENV === 'production') {
            raven.captureException(err, {
              extra: {
                user: req.user,
                method: req.method,
                headers: req.headers,
                route: route,
                params: req.params,
                query: req.query,
                body: req.body
              }
            })
          }
        }
      })

      this.server = server

      return resolve(server)
    })
  }

  listen () {
    return new Promise((resolve, reject) => {
      this.server.listen(process.env.PORT, (err) => {
        if (!_.isNil(err)) {
          return reject(err)
        } else {
          console.log(`HTTP Server is now listening at port ${process.env.PORT}`)
          return resolve()
        }
      })
    })
  }

  close () {
    this.logger.info('Closing HTTP Server.')

    return new Promise((resolve, reject) => {
      this.server.removeAllListeners()

      this.server.close((err) => {
        if (!_.isNil(err)) {
          reject(err)
        } else {
          this.logger.info('HTTP Server closed.')

          resolve()
        }
      })
    })
  }
}

module.exports = HttpServer
