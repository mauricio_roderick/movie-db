'use strict'

let bluebird = require('bluebird')
let mongoose = require('mongoose')

global.appVersion = {}
global.Promise = bluebird
mongoose.Promise = bluebird

let _ = require('lodash')
let dotenv = require('dotenv')

let System = require('./system')
let Application = require('./app')

if (process.env.NODE_ENV !== 'production') {
  dotenv.config({path: `./config/env/${process.env.NODE_ENV}.env`})
}

let system = new System()
let application = new Application()

Promise.props({
  db: system.database.bootstrap(),
  httpServer: system.httpServer.bootstrap()
}).then((_system) => {
  return application.bootstrap(_system)
}).then(() => {
  return system.httpServer.listen()
}).then(() => {
  console.log('Application bootstrap successful.')
}).catch(e => {
  console.log(e)
})


// let mongoose = require('mongoose')
// let options = {
// 	useMongoClient: true,
//     promiseLibrary: Promise,
//     keepAlive: 10000
// }
// mongoose.connect('mongodb://db-admin:Ek74Bb7ER3teADq9eTKMu67K@movie-shard-00-00-guqxx.mongodb.net:27017,movie-shard-00-01-guqxx.mongodb.net:27017,movie-shard-00-02-guqxx.mongodb.net:27017/test?replicaSet=movie-shard-0&ssl=true&authSource=admin', options)
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error:'));
// db.once('open', function() {
//   console.log('we\'re connected!')
// });