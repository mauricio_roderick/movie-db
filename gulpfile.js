'use strict'

const PATHS = {
  js: ['*.js', '*/*.js', '*/**/*.js', '!node_modules/**', '!gulpfile.js'],
  json: ['*.json', '*/*.json', '*/**/*.json', '!node_modules/**'],
  specs: ['app/resources/**/spec.js']
}

let env = require('gulp-env')
let gulp = require('gulp')
let mocha = require('gulp-mocha')
let jshint = require('gulp-jshint')
let nodemon = require('gulp-nodemon')
let plumber = require('gulp-plumber')
let jsonlint = require('gulp-json-lint')
let standard = require('gulp-standard')

let runApp = function (nodeEnv = 'development') {
  env({
    vars: {
      NODE_ENV: nodeEnv
    }
  })

  nodemon({
    script: 'index.js',
    ext: 'js',
    watch: PATHS.js,
    ignore: ['node_modules', '.git', '.gitignore', '.jshintrc'],
    restartable: 'rs'
  })
}

gulp.task('js-lint', function () {
  return gulp.src(PATHS.js)
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'))
})

gulp.task('json-lint', function () {
  return gulp.src(PATHS.json)
    .pipe(plumber())
    .pipe(jsonlint({
      comments: true
    }))
    .pipe(jsonlint.report())
})

gulp.task('standard', function () {
  return gulp.src(PATHS.js)
    .pipe(standard())
    .pipe(standard.reporter('default', {
      breakOnError: true,
      quiet: true
    }))
})

gulp.task('run-tests', function () {
  env({
    vars: {
      NODE_ENV: 'test'
    }
  })

  return gulp.src(PATHS.specs)
    .pipe(mocha({reporter: 'spec'}))
})

gulp.task('run-test-env', () => runApp('test'))
gulp.task('run', () => runApp())
gulp.task('lint', ['js-lint', 'json-lint', 'standard'])
gulp.task('test', ['lint', 'run-tests'])
gulp.task('default', ['run'])
